import { html, css, LitElement } from 'lit-element';

export class ApiEmployees extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--api-employees-text-color, #000);
      }
      td#employeename, td#ename{
  text-align: left;
  border-bottom: 1px solid #BCBCBC;

      }
      .tabledesign{
        width: 100%;
        border: none;
      }

      td#employeelast, td#elast{
        text-align: left;
        border-bottom: 1px solid #BCBCBC;

      }

      td#employeesalary, td#salaryt{
        text-align: right;
        border-bottom: 1px solid #BCBCBC;

      }

      .botones{
        text-align: right;
      }

      table tr:nth-child(even){
        background-color: #eee;
      }

      table tr:nth-child(odd){
        background-color: #fff;
      }

      .ok, .cancel{
        text-align: right;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
    };
  }

  constructor() {
    super();
  }

  clear(){
    alert('si sirve');
    this.shadowRoot.querySelector("#inputid").value="";
  }
  okk(){
        this.shadowRoot.querySelector("#inputid").value = "";
      }
  cncel(){
        this.shadowRoot.querySelector("#inputid").value = "";
      }
  search(){
         let idinput = this.shadowRoot.querySelector("input").value;
         let ename = this.shadowRoot.querySelector("#ename");
         let elast = this.shadowRoot.querySelector("#elast");
         let salaryt = this.shadowRoot.querySelector('#salaryt');

         fetch('http://dummy.restapiexample.com/api/v1/employees')
         .then(function(response){
          return response.json();
         })
         .then(function(myJson){
          console.log(myJson);
            //alert('valor de myJson: ' + myJson);
            let object = myJson;
            let array = new Array();
            let salary = new Array();
            let counter = 1;

            for (let i = 0; i < 24; i++){
              array[counter] = object['data'][i]['employee_name'];
              salary[counter] = object['data'][i]['employee_salary'];
              counter++;
            }
            console.log(array);
            console.log(salary);


            if(idinput <= 24 && idinput > 0){
              let space = array[idinput].split(' ');
              let name = space[0];
              let names = space[1];
              elast.innerHTML= names;
              ename.innerHTML = name;
              salaryt.innerHTML = salary[idinput];
            }else{
              alert('agregar un id del 1 a 24');
            }

         });
      }

  

  render() {
    return html`
      Filter: 
    <input type="number" id="inputid" placeholder="Ingresar un ID">
    <button id="searchbtn" @click="${this.search}">Search</button>
    &nbsp;
    <button id="clearbtn" @click="${this.clear}">Clear</button>
      <hr>
      <table class="tabledesign" id="table-employee">
      <tr>
        <td colspan="3">Last Moth New Customers</td>
      </tr>
      <tr>
        <td id="employeename">Nombre</td>
        <td id="employeelast">Apellido</td>
        <td id="employeesalary">Amount (YEN)</td>
      </tr>
      <tr>
        <td id="ename"></td>
        <td id="elast"></td>
        <td id="salaryt"></td>
      </tr>
    </table>
    <br>
    <br>
    <div class="botones">
    <button id="okbtn" @click="${this.okk}">OK</button>
    <button id="cancelbtn" @click="${this.cncel}">Cancel</button>
    </div>
    `;
  }
}
